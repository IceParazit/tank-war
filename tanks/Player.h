#pragma once
#include "includes.h"
using namespace sf;

class Player
{
private:
	enum { left, right, down, up} state;
	String name;
	Texture tankBodyTex;
	Sprite tankBodySprt;
	Texture tankTowerTex;
	Sprite tankTowerSprt;
	Vector2i pixelPos;
	Vector2f pos;
	float x;
	float y;
	float dX;
	float dY;
	float speed;
	int Bw;
	int Bh;
	int Tw;
	int Th;
	int BodyRot;
	int bodyState;// 0 - up, 1 - down, 2 - left, 3 - right.
	int bodyStateAngle;

public:
	View view;
	Player(Image &body, Image &tower, String Name, float X, float Y, int BW, int BH, int TW, int TH);
	~Player();
	void control();
	void towerControl(RenderWindow &window);
	void update(float timer, String *Map, RenderWindow &window);
	void checkCollisionWithMap(float Dx, float Dy, String *Map);
	void controlBody();
	Sprite getSpriteBody();
	Sprite getSpriteTower();
	View getplayercoordinateforview(float x, float y);
};

