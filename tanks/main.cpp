#include "includes.h"

const int HEIGHT_MAP = 25;//������ ����� ������
const int WIDTH_MAP = 40;//������ ����� ������ 


sf::String TileMap[HEIGHT_MAP] = {
	"0000000000000000000000000000000000000000",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0                                      0",
	"0      00000000000000000000            0",
	"0      0                               0",
	"0      0         00000000              0",
	"0      0                0              0",
	"0      00000000000      0              0",
	"0      0                0              0",
	"0      0     000000000000              0",
	"0      0                0              0",
	"0000000000000000000000000000000000000000",
};

int main()
{
	Clock clock;
	Image map_image;
	map_image.loadFromFile("map.png");
	Texture map;
	map.loadFromImage(map_image);
	Sprite s_map;
	s_map.setTexture(map);
	Image Tower;
	Image Body;
	Tower.loadFromFile("tower.png");
	Tower.createMaskFromColor(Color(255, 255, 255));
	Body.loadFromFile("body.png");

	Player p(Body, Tower, "tank", 100, 100, 32, 32, 16, 20);

	RenderWindow window(VideoMode(640, 480), "Tanks War");
	p.view.reset(FloatRect(0, 0, 640, 480));



	while (window.isOpen())
	{
		float timer = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		timer = timer / 800;
		
		Event event;
		while (window.pollEvent(event))
		{
			p.controlBody();
			if (event.type == sf::Event::Closed)
				window.close();
		}
		p.update(timer, TileMap, window);
		window.setView(p.view);
		window.clear(Color(77, 83, 140));

		for (int i = 0; i < HEIGHT_MAP; i++)
			for (int j = 0; j < WIDTH_MAP; j++)
			{
				if (TileMap[i][j] == ' ')  s_map.setTextureRect(IntRect(0, 0, 32, 32));
				if (TileMap[i][j] == 's')  s_map.setTextureRect(IntRect(32, 0, 32, 32));
				if ((TileMap[i][j] == '0')) s_map.setTextureRect(IntRect(64, 0, 32, 32));

				s_map.setPosition(j * 32, i * 32);

				window.draw(s_map);
			}
		window.draw(p.getSpriteBody());
		window.draw(p.getSpriteTower());
		window.display();
	}

	return 0;
}