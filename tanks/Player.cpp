#include "Player.h"



Player::Player(Image &body, Image &tower, String Name, float X, float Y, int BW, int BH, int TW, int TH)
{
	x = X;
	y = Y;
	dX = 0;
	dY = 0;
	Bw = BW;
	Bh = BH;
	Tw = TW;
	Th = TH;
	name = Name;
	tankBodyTex.loadFromImage(body);
	tankTowerTex.loadFromImage(tower);
	tankBodySprt.setTexture(tankBodyTex);
	tankTowerSprt.setTexture(tankTowerTex);
	tankBodySprt.setTextureRect(IntRect(0, 0, Bw, Bh));
	tankTowerSprt.setTextureRect(IntRect(0, 0, Tw, Th));
	tankBodySprt.setOrigin(Bw / 2, Bh / 2);
	tankTowerSprt.setOrigin(Tw / 2, (Th / 2) + 4);
	bodyState = 0;
	bodyStateAngle = 0;
}


Player::~Player()
{
}


Sprite Player::getSpriteBody()
{
	return Sprite(tankBodySprt);
}

Sprite Player::getSpriteTower()
{
	return Sprite(tankTowerSprt);
}

void Player::checkCollisionWithMap(float Dx, float Dy, String * Map)
{
	for (int i = y / 32; i < (y + Bh) / 32; i++)
		for (int j = x / 32; j < (x + Bw) / 32; j++)
		{
			if (Map[i][j] == '0')
			{
				if (Dy > 0) { y = i * 32 - Bh;  dY = 0; }
				if (Dy < 0) { y = i * 32 + 32;  dY = 0; }
				if (Dx > 0) { x = j * 32 - Bw; }
				if (Dx < 0) { x = j * 32 + 32; }

			}

		}
}
void Player::controlBody() 
{
	if (Keyboard::isKeyPressed(Keyboard::Left)) 
	{
		switch (bodyState)
		{
		case 0:
			bodyState = 2;
			tankBodySprt.setRotation(-90);
			break;
		case 1:
			bodyState = 3;
			tankBodySprt.setRotation(90);
			break;
		case 2:
			bodyState = 1;
			tankBodySprt.setRotation(180);
			break;
		case 3:
			bodyState = 0;
			tankBodySprt.setRotation(0);
			break;
		}
	}
	if (Keyboard::isKeyPressed(Keyboard::Right)) 
	{
		switch (bodyState)
		{
		case 0:
			bodyState = 3;
			tankBodySprt.setRotation(90);
			break;
		case 1:
			bodyState = 2;
			tankBodySprt.setRotation(-90);
			break;
		case 2:
			bodyState = 0;
			tankBodySprt.setRotation(0);
			break;
		case 3:
			bodyState = 1;
			tankBodySprt.setRotation(180);
			break;
		}
	}
}

void Player::control()
{
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		switch (bodyState)
		{
		case 0:
			state = up;
			speed = 0.1;
			break;
		case 1:
			state = down;
			speed = 0.1;
			break;
		case 2:
			state = left;
			speed = 0.1;
			break;
		case 3:
			state = right;
			speed = 0.1;
			break;
		}
	}

	if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		switch (bodyState)
		{
		case 0:
			state = down;
			speed = 0.05;
			break;
		case 1:
			state = up;
			speed = 0.05;
			break;
		case 2:
			state = right;
			speed = 0.05;
			break;
		case 3:
			state = left;
			speed = 0.05;
			break;
		}

	}
}

void Player::towerControl(RenderWindow &window)
{
	Vector2i pixelPos = Mouse::getPosition(window);//�������� ����� �������
	Vector2f pos = window.mapPixelToCoords(pixelPos);//��������� �� � ������� (������ �� ����� ����)
	float dX = pos.x - x; float dY = pos.y - y;//�� ��, ���������� y
	float rotation = (atan2(dY, dX)) * 180 / 3.14159265;//�������� ���� � �������� � ��������� ��� � �������
	std::cout << rotation << "\n";//������� �� ������� � ���������
	tankTowerSprt.setRotation(rotation + 90);//������������ ������ �� ��� �������
}

void Player::update(float timer, String *Map, RenderWindow &window)
{
	control();
	towerControl(window);
	dY = 0;
	dX = 0;



	switch (state)
	{
	case Player::left:
		dX = -speed;
		break;
	case Player::right:
		dX = speed;
		break;
	case Player::up:
		dY = -speed;
		break;
	case Player::down:
		dY = speed;
		break;
	}
	

	speed = 0;
	tankBodySprt.setPosition(x + Bw / 2, y + Bh / 2);
	tankTowerSprt.setPosition((x + Tw / 2)+8, (y + Th / 2) + 6);
	getplayercoordinateforview(x, y);
	x += dX * timer;
	checkCollisionWithMap(dX, 0, Map);
	y += dY * timer;
	checkCollisionWithMap(0, dY, Map);
	checkCollisionWithMap(0, dY, Map);

}

View Player::getplayercoordinateforview(float x, float y)
{
	float tempX = x, tempY = y;
	if (x < 320) tempX = 320;
	if (y < 240) tempY = 240;
	if (y > 554) tempY = 554;
	view.setCenter(tempX, tempY);
	return View(view);
}